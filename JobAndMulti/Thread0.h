#pragma once
#ifndef THREAD0_H
#define THREAD0_H

#include "ThreadContainer.h"

class Thread0
{
public:
	Thread0();
	~Thread0();

	int Add(int a, int b);
};

class Thread0Container : 
	public TypedThreadContainer<Thread0, ThreadType::TT_Thread0>
{
protected:

public:
	virtual ThreadType GetType() const { return Type; }

	Thread0Container(JobManager& jobManager, Thread0& worker) : 
		TypedThreadContainer(jobManager, worker) {}
	virtual ~Thread0Container() {};

	virtual void RunThread();
	virtual std::thread& Spawn();
};







class T0Command : public ThreadCommand {
public:
	Thread0Container & t0Cont;
	T0Command(Thread0Container& _t0Cont) :
		ThreadCommand(ThreadType::TT_Thread0), t0Cont(_t0Cont)
	{}
	virtual bool execute() const = 0;
};

class T0AddCommand : public T0Command {
public:
	int left, right;
	T0AddCommand(Thread0Container& _t0Cont, int _left, int _right) :
		T0Command(_t0Cont), left(_left), right(_right)
	{}
	virtual bool execute() const;
};

class T0TerminateCommand : public T0Command {
public:
	T0TerminateCommand(Thread0Container& _t0Cont) :
		T0Command(_t0Cont)
	{}
	virtual bool execute() const;
};

#endif