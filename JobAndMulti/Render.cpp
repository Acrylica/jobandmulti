#include "Render.h"
#include "JobManager.h"
#include <iostream>

Render::Render(JobManager& _jobManager) : busy(false), terminate(false), jobManager(_jobManager) {}

Render::~Render(){}

void Render::RenderThread()
{
	while (!terminate) {
		if (!busy)
			std::cout << "Idling" << std::endl;
		else {
			DoWork();
		}
	}
}

void Render::StartWork(int data)
{
	std::cout << "Work received!" << std::endl;
	mData = data;
	busy = true;
}

void Render::DoWork()
{
	std::cout << "Work finished! : " << mData << std::endl;
	busy = false;
}

std::thread Render::Spawn()
{
	return std::thread(&Render::RenderThread, this);
}



