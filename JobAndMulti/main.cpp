#include <windows.h>
#include <thread>
#include "Global.h"
#include "Thread0.h"
#include "Thread1.h"
#include "Thread2.h"
#include "Thread3.h"

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	THREAD_JOB_MNGR.Init();
	// Spawn 4 threads
	// Register each with THREAD_JOB_MNGR
	// run while loop here <- "Main Thread"
	//Thread0Container* t0Cont = new Thread0Container(THREAD_JOB_MNGR, THREAD_0);
	THREAD_JOB_MNGR.RegisterThreadContainer(new Thread0Container(THREAD_JOB_MNGR, THREAD_0));
	//std::thread& Thread0Thread = t0Cont->Spawn();

	//Thread1Container* t1Cont = new Thread1Container(THREAD_JOB_MNGR, THREAD_1);
	THREAD_JOB_MNGR.RegisterThreadContainer(new Thread1Container(THREAD_JOB_MNGR, THREAD_1));
	//std::thread& Thread1Thread = t1Cont->Spawn();

	//Thread2Container* t2Cont = new Thread2Container(THREAD_JOB_MNGR, THREAD_2);
	THREAD_JOB_MNGR.RegisterThreadContainer(new Thread2Container(THREAD_JOB_MNGR, THREAD_2));
	///*std::thread& Thread2Thread = */t2Cont->Spawn();

	//Thread3Container* t3Cont = new Thread3Container(THREAD_JOB_MNGR, THREAD_3);
	THREAD_JOB_MNGR.RegisterThreadContainer(new Thread3Container(THREAD_JOB_MNGR, THREAD_3));
	///*std::thread& Thread3Thread = */t3Cont->Spawn();

	unsigned long count = 0;

	while (++count < 1000) { 
		if (count % 100 == 0) {
			THREAD_JOB_MNGR.AddNewJob(new T0AddCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread0Container>(ThreadType::TT_Thread0), count, count / 2));
			//THREAD_JOB_MNGR.AddNewJob(new T0AddCommand(*t0Cont, count, count / 2));
		}

		if (count % 50 == 0) {
			THREAD_JOB_MNGR.AddNewJob(new T1SubtractCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread1Container>(ThreadType::TT_Thread1), count, count / 2));
			//THREAD_JOB_MNGR.AddNewJob(new T1SubtractCommand(*t1Cont, count, count / 2));
		}

		if (count % 300 == 0) {
			THREAD_JOB_MNGR.AddNewJob(new T2SpawnWorkCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread2Container>(ThreadType::TT_Thread2), count / 3));
		}
	}
	THREAD_JOB_MNGR.AddNewJob(new T0TerminateCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread0Container>(ThreadType::TT_Thread0)));
	THREAD_JOB_MNGR.AddNewJob(new T1TerminateCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread1Container>(ThreadType::TT_Thread1)));
	THREAD_JOB_MNGR.AddNewJob(new T2TerminateCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread2Container>(ThreadType::TT_Thread2)));
	while (THREAD_JOB_MNGR.HasAnyJobs()) {

	}

	THREAD_JOB_MNGR.AddNewJob(new T3TerminateCommand(*THREAD_JOB_MNGR.GetThreadContainer<Thread3Container>(ThreadType::TT_Thread3)));
	for (int i = 0; i < ThreadType::NUMTHREADTYPES - 1; ++i) {
		THREAD_JOB_MNGR.GetGenericThreadContainer(ThreadType(i))->Join();
	}

	//Render* r = new Render(JobMngr);
	//std::thread RenderThread = r.Spawn();

	//for (int i = 0; i < 50; ++i) {
	//	JobMngr.AddNewJob(new RenderWorkCommand(r, i));
	//}
	//JobMngr.AddNewJob(new TerminateRenderCommand(r));
	//while (JobMngr.HasJobs()) {
	//	JobMngr.Update(2);
	//}

	//RenderThread.join();
	return 0;
}

