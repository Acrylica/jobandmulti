#include "Thread3.h"
#include "Global.h"
#include <iostream>

Thread3::Thread3() {}

Thread3::~Thread3() {}

void Thread3::ReceiveWork(std::string sender)
{
	std::cout << "Thread3 : Received work from: " << sender.c_str() << std::endl;
}

void Thread3Container::RunThread()
{
	while (!m_Terminate) {
		if (!mp_CurrentCommand) {
			if (m_JobManager.HasJobs(Type)) {
				mp_CurrentCommand = m_JobManager.GetThreadJob(Type);
				std::cout << "Thread3 : Work received." << std::endl;
			}
			std::cout << "Thread3 : Idling..." << std::endl;
		}

		if (mp_CurrentCommand) {
			if (mp_CurrentCommand->execute()) {
				std::cout << "Thread3 : Work completed." << std::endl;
				m_JobManager.FinishedJob(Type);
				mp_CurrentCommand = nullptr;
			}
			std::cout << "Thread3 : Working..." << std::endl;
		}
	}
	std::cout << "Thread3 : Terminating." << std::endl;
	//Join();
}

std::thread& Thread3Container::Spawn()
{
	m_Thread = new std::thread(&Thread3Container::RunThread, this);
	return *m_Thread;
}

bool T3ReceiveWorkCommand::execute() const
{
	if (t3Cont.IsBusy())
		return false;
	t3Cont.Worker().ReceiveWork(sender);
	return true;
}

bool T3TerminateCommand::execute() const
{
	if (t3Cont.IsBusy())
		return false;
	t3Cont.Terminate();
	return true;
}
