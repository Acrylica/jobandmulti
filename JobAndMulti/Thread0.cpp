#include "Thread0.h"
#include "JobManager.h"
#include <iostream>

Thread0::Thread0(){}

Thread0::~Thread0(){}

int Thread0::Add(int a, int b)
{
	int c = a + b;
	std::cout << "Thread0 : Added: " << a << " + " << b << " = " << c << std::endl;
	return c;
}

void Thread0Container::RunThread()
{
	while (!m_Terminate) {
		if (!mp_CurrentCommand) {
			if (m_JobManager.HasJobs(Type)) {
				mp_CurrentCommand = m_JobManager.GetThreadJob(Type);
				std::cout << "Thread0 : Work received." << std::endl;
			}
			std::cout << "Thread0 : Idling..." << std::endl;
		}

		if (mp_CurrentCommand) {
			if (mp_CurrentCommand->execute()) {
				std::cout << "Thread0 : Work completed." << std::endl;
				m_JobManager.FinishedJob(Type);
				mp_CurrentCommand = nullptr;
			}
			std::cout << "Thread0 : Working..." << std::endl;
		}
	}
	std::cout << "Thread0 : Terminating." << std::endl;
	//Join();
}

std::thread& Thread0Container::Spawn()
{
	m_Thread = new std::thread(&Thread0Container::RunThread, this);
	return *m_Thread;
}

bool T0AddCommand::execute() const
{
	if (t0Cont.IsBusy())
		return false;
	t0Cont.Worker().Add(left, right);
	return true;
}

bool T0TerminateCommand::execute() const
{
	if (t0Cont.IsBusy())
		return false;
	t0Cont.Terminate();
	return true;
}
