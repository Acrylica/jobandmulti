#include "JobManager.h"

CommandNode::~CommandNode() 
{
	delete mp_Cmd;
}

void CommandNode::Clean()
{
	if (mp_Next)
		mp_Next->Clean();
	delete mp_Cmd;
	delete mp_Next;
}



CommandNodeContainer::~CommandNodeContainer()
{
	if (mp_Head)
		mp_Head->Clean();
	mp_Head = mp_Tail = 0;
}

void CommandNodeContainer::AddCommand(ThreadCommand * cmd)
{
	CommandNode* newCommandNode = new CommandNode(cmd);
	// If the head is null, the tail is too, meaning we have an empty list
	if (!mp_Head) {
		mp_Head = mp_Tail = newCommandNode;
	}
	else {
		mp_Tail->mp_Next = newCommandNode;
		mp_Tail = mp_Tail->mp_Next;
	}
}

ThreadCommand * CommandNodeContainer::GetTopCommand()
{
	return mp_Head->mp_Cmd;
}

void CommandNodeContainer::PopTopCommand()
{
	CommandNode * newHead = mp_Head->mp_Next;
	delete mp_Head;
	mp_Head = newHead;
}

bool CommandNodeContainer::HasCommands()
{
	return mp_Head;
}











JobManager::JobManager()
{}

JobManager::~JobManager()
{
	for (int i = 0; i < ThreadType::NUMTHREADTYPES; ++i) {
		delete m_CommandQueues[i];
	}
	//delete[] m_CommandQueues;
}

void JobManager::Init()
{
	for (int i = 0; i < ThreadType::NUMTHREADTYPES-1; ++i) {
		m_ThreadContainers[i] = nullptr;
	}
	for (int i = 0; i < ThreadType::NUMTHREADTYPES; ++i) {
		m_CommandQueues[i] = new CommandNodeContainer();
	}
}

void JobManager::RegisterThreadContainer(ThreadContainer* tjc)
{
	m_ThreadContainers[tjc->GetType()] = tjc;
	tjc->Spawn();
}

ThreadContainer * JobManager::GetGenericThreadContainer(ThreadType type)
{
	return m_ThreadContainers[type];
}

void JobManager::AddNewJob(ThreadCommand * cmd)
{
	//m_CommandQueues[cmd->type].push(cmd);
	m_CommandQueues[cmd->type]->AddCommand(cmd);
}

void JobManager::FinishedJob(ThreadType type)
{
	m_CommandQueues[type]->PopTopCommand();
}

bool JobManager::HasJobs(ThreadType type)
{
	//return !m_CommandQueues[type].empty();
	return m_CommandQueues[type]->HasCommands();
}

bool JobManager::HasAnyJobs()
{
	bool hasAnyJobs = false;
	for (int i = 0; i < ThreadType::NUMTHREADTYPES; ++i) {
		hasAnyJobs = HasJobs(ThreadType(i)) ? true : hasAnyJobs;
	}
	return hasAnyJobs;
}

const ThreadCommand * JobManager::GetThreadJob(ThreadType type)
{
	//ThreadCommand* cmd =  m_CommandQueues[type].top();
	//m_CommandQueues[type].pop();
	//return cmd;

	return m_CommandQueues[type]->GetTopCommand();
}
