#pragma once
#ifndef THREAD_JOB_CONTAINER_H
#define THREAD_JOB_CONTAINER_H

#include "ThreadType.h"
#include "Command.h"
#include <queue>
#include <vector>
#include <thread>

class JobManager;

class ThreadContainer
{
protected:
	JobManager & m_JobManager;
	std::thread * m_Thread;
	bool m_Terminate;
	bool m_Busy;
	const ThreadCommand* mp_CurrentCommand;

public:
	static const ThreadType Type = ThreadType::AnyThread;
	virtual ThreadType GetType() const { return Type; }

	ThreadContainer(JobManager& jobManager) : 
		m_JobManager(jobManager), m_Thread(nullptr),
		m_Terminate(false), m_Busy(false),
		mp_CurrentCommand(nullptr)
	{};
	virtual ~ThreadContainer() {};

	virtual void RunThread() = 0;
	virtual std::thread& Spawn() = 0;
	void Join() {
		m_Thread->join();
	}
	inline void Terminate() { m_Terminate = true; }
	inline bool IsBusy() { return m_Busy; }
};

template<typename ThreadWorker, ThreadType type>
class TypedThreadContainer : public ThreadContainer {
protected:
	ThreadWorker & m_Worker;

public:
	static const ThreadType Type = type;
	virtual ThreadType GetType() const { return Type; }

	TypedThreadContainer(JobManager& jobManager, ThreadWorker& worker) :
		ThreadContainer(jobManager),
		m_Worker(worker) {}
	virtual ~TypedThreadContainer() {};

	virtual void RunThread() = 0; 
	virtual std::thread& Spawn() = 0;

	inline ThreadWorker& Worker() { return m_Worker; }
};



#endif