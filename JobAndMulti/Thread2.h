#pragma once
#ifndef THREAD2_H
#define THREAD2_H

#include "ThreadContainer.h"

class Thread2
{
public:
	Thread2();
	~Thread2();

	void SpawnWork(int a);
};

class Thread2Container :
	public TypedThreadContainer<Thread2, ThreadType::TT_Thread2>
{
protected:

public:
	virtual ThreadType GetType() const { return Type; }

	Thread2Container(JobManager& jobManager, Thread2& worker) :
		TypedThreadContainer(jobManager, worker) {}
	virtual ~Thread2Container() {};

	virtual void RunThread();
	virtual std::thread& Spawn();
};







class T2Command : public ThreadCommand {
public:
	Thread2Container & t2Cont;
	T2Command(Thread2Container& _t2Cont) :
		ThreadCommand(ThreadType::TT_Thread2), t2Cont(_t2Cont)
	{}
	virtual bool execute() const = 0;
};

class T2SpawnWorkCommand : public T2Command {
public:
	int data;
	T2SpawnWorkCommand(Thread2Container& _t2Cont, int _data) :
		T2Command(_t2Cont), data(_data)
	{}
	virtual bool execute() const;
};

class T2TerminateCommand : public T2Command {
public:
	T2TerminateCommand(Thread2Container& _t2Cont) :
		T2Command(_t2Cont)
	{}
	virtual bool execute() const;
};

#endif