#include "Thread2.h"
#include "Global.h"
#include <iostream>

Thread2::Thread2() {}

Thread2::~Thread2() {}

void Thread2::SpawnWork(int a)
{
	std::cout << "Thread2 : Added a new command: " << a << std::endl;
	THREAD_JOB_MNGR.AddNewJob(
		new T3ReceiveWorkCommand(
			*THREAD_JOB_MNGR.GetThreadContainer<Thread3Container>(ThreadType::TT_Thread3),
			std::string("Thread2"))
	);
}

void Thread2Container::RunThread()
{
	while (!m_Terminate) {
		if (!mp_CurrentCommand) {
			if (m_JobManager.HasJobs(Type)) {
				mp_CurrentCommand = m_JobManager.GetThreadJob(Type);
				std::cout << "Thread2 : Work received." << std::endl;
			}
			std::cout << "Thread2 : Idling..." << std::endl;
		}

		if (mp_CurrentCommand) {
			if (mp_CurrentCommand->execute()) {
				std::cout << "Thread2 : Work completed." << std::endl;
				m_JobManager.FinishedJob(Type);
				mp_CurrentCommand = nullptr;
			}
			std::cout << "Thread2 : Working..." << std::endl;
		}
	}
	std::cout << "Thread2 : Terminating." << std::endl;
	//Join();
}

std::thread& Thread2Container::Spawn()
{
	m_Thread = new std::thread(&Thread2Container::RunThread, this);
	return *m_Thread;
}

bool T2SpawnWorkCommand::execute() const
{
	if (t2Cont.IsBusy())
		return false;
	t2Cont.Worker().SpawnWork(data);
	return true;
}

bool T2TerminateCommand::execute() const
{
	if (t2Cont.IsBusy())
		return false;
	t2Cont.Terminate();
	return true;
}
