#pragma once

#ifndef RENDER_H
#define RENDER_H
#include <thread>
#include "Command.h"

class JobManager;
class Render
{
protected:
	bool busy;
	bool terminate;
	JobManager& jobManager;
	int mData;
public:
	Render(JobManager& _jobManager);
	~Render();

	void RenderThread();
	void StartWork(int data);
	void DoWork();
	inline bool IsBusy() {
		return busy;
	}
	inline void Terminate() {
		terminate = true;
	}

	std::thread Spawn();
};


#endif