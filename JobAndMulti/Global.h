#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H

#include "Singleton.h"
#include "JobManager.h"
#include "Thread0.h"
#include "Thread1.h"
#include "Thread2.h"
#include "Thread3.h"

#define THREAD_JOB_MNGR Singleton<JobManager>::GetInstance()
#define THREAD_0 Singleton<Thread0>::GetInstance()
#define THREAD_1 Singleton<Thread1>::GetInstance()
#define THREAD_2 Singleton<Thread2>::GetInstance()
#define THREAD_3 Singleton<Thread3>::GetInstance()

#endif