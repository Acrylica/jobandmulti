#pragma once
#ifndef THREADTYPE_H
#define THREADTYPE_H

enum ThreadType {
	TT_Thread0 = 0,
	TT_Thread1,
	TT_Thread2,
	TT_Thread3,
	AnyThread,
	NUMTHREADTYPES
};

#endif