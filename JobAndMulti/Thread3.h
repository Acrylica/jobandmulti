#pragma once
#ifndef THREAD3_H
#define THREAD3_H

#include "ThreadContainer.h"

class Thread3
{
public:
	Thread3();
	~Thread3();

	void ReceiveWork(std::string sender);
};

class Thread3Container :
	public TypedThreadContainer<Thread3, ThreadType::TT_Thread3>
{
protected:

public:
	virtual ThreadType GetType() const { return Type; }

	Thread3Container(JobManager& jobManager, Thread3& worker) :
		TypedThreadContainer(jobManager, worker) {}
	virtual ~Thread3Container() {};

	virtual void RunThread();
	virtual std::thread& Spawn();
};







class T3Command : public ThreadCommand {
public:
	Thread3Container & t3Cont;
	T3Command(Thread3Container& _t3Cont) :
		ThreadCommand(ThreadType::TT_Thread3), t3Cont(_t3Cont)
	{}
	virtual bool execute() const = 0;
};

class T3ReceiveWorkCommand : public T3Command {
public:
	std::string sender;
	T3ReceiveWorkCommand(Thread3Container& _t3Cont, std::string _sender) :
		T3Command(_t3Cont), sender(_sender)
	{}
	virtual bool execute() const;
};

class T3TerminateCommand : public T3Command {
public:
	T3TerminateCommand(Thread3Container& _t3Cont) :
		T3Command(_t3Cont)
	{}
	virtual bool execute() const;
};

#endif