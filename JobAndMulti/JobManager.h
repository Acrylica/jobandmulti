#pragma once
#ifndef JOB_MANAGER_H
#define JOB_MANAGER_H

#include <queue>
#include <vector>
#include <thread>
#include <unordered_map>
#include "Command.h"
#include "ThreadContainer.h"

class CommandNodeContainer;
class CommandNode {
protected:
	ThreadCommand* mp_Cmd;
	CommandNode* mp_Next;

public:
	friend CommandNodeContainer;
	CommandNode(ThreadCommand* cmd) : 
		mp_Cmd(cmd), mp_Next(nullptr) {}
	~CommandNode();

	void Clean();
};

class CommandNodeContainer {
protected:
	CommandNode* mp_Head = nullptr;
	CommandNode* mp_Tail = nullptr;

public:
	CommandNodeContainer() : 
		mp_Head(nullptr), mp_Tail(nullptr) {}
	~CommandNodeContainer();
	void AddCommand(ThreadCommand* cmd);
	ThreadCommand* GetTopCommand();
	void PopTopCommand();
	bool HasCommands();
};


class JobManager
{
protected:
	// Store a number of ThreadContainers equal the total number of thread types - 1 as the final thread type is 'Any'
	// and does not correspond to any specific ThreadContainer
	ThreadContainer * m_ThreadContainers[ThreadType::NUMTHREADTYPES-1];

	// Store a number of priotity_queues equal to the total number of thread types
	// Note that the 'Any' queue does not correspond to any specific ThreadContainer and when processed, will
	// spawn a new mini thread
	//std::priority_queue<ThreadCommand*, std::vector<ThreadCommand*>, hasPriority> m_CommandQueues[ThreadType::NUMTHREADTYPES];
	CommandNodeContainer* m_CommandQueues[ThreadType::NUMTHREADTYPES];

public:
	JobManager();
	~JobManager();

	void Init();
	void RegisterThreadContainer(ThreadContainer* tjc);

	template <typename T>
	T* GetThreadContainer(ThreadType type) {
		return static_cast<T*>(m_ThreadContainers[type]);
	}

	ThreadContainer* GetGenericThreadContainer(ThreadType type);
	void AddNewJob(ThreadCommand * cmd);
	void FinishedJob(ThreadType type);
	bool HasJobs(ThreadType type);
	bool HasAnyJobs();
	const ThreadCommand * GetThreadJob(ThreadType type);
};

#endif JOB_MANAGER_H