#include "Thread1.h"
#include "JobManager.h"
#include <iostream>

Thread1::Thread1() {}

Thread1::~Thread1() {}

int Thread1::Subtract(int a, int b)
{
	int c = a + b;
	std::cout << "Thread1 : Subtracted: " << a << " + " << b << " = " << c << std::endl;
	return c;
}

void Thread1Container::RunThread()
{
	while (!m_Terminate) {
		if (!mp_CurrentCommand) {
			if (m_JobManager.HasJobs(Type)) {
				mp_CurrentCommand = m_JobManager.GetThreadJob(Type);
				std::cout << "Thread1 : Work received." << std::endl;
			}
			std::cout << "Thread1 : Idling..." << std::endl;
		}

		if (mp_CurrentCommand) {
			if (mp_CurrentCommand->execute()) {
				std::cout << "Thread1 : Work completed." << std::endl;
				m_JobManager.FinishedJob(Type);
				mp_CurrentCommand = nullptr;
			}
			std::cout << "Thread1 : Working..." << std::endl;
		}
	}
	std::cout << "Thread1 : Terminating." << std::endl;
	//Join();
}

std::thread& Thread1Container::Spawn()
{
	m_Thread = new std::thread(&Thread1Container::RunThread, this);
	return *m_Thread;
}

bool T1SubtractCommand::execute() const
{
	if (t1Cont.IsBusy())
		return false;
	t1Cont.Worker().Subtract(left, right);
	return true;
}

bool T1TerminateCommand::execute() const
{
	if (t1Cont.IsBusy())
		return false;
	t1Cont.Terminate();
	return true;
}
