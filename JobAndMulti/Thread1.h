#pragma once
#ifndef THREAD1_H
#define THREAD1_H

#include "ThreadContainer.h"

class Thread1
{
public:
	Thread1();
	~Thread1();

	int Subtract(int a, int b);
};

class Thread1Container :
	public TypedThreadContainer<Thread1, ThreadType::TT_Thread1>
{
protected:

public:
	virtual ThreadType GetType() const { return Type; }

	Thread1Container(JobManager& jobManager, Thread1& worker) :
		TypedThreadContainer(jobManager, worker) {}
	virtual ~Thread1Container() {};

	virtual void RunThread();
	virtual std::thread& Spawn();
};







class T1Command : public ThreadCommand {
public:
	Thread1Container & t1Cont;
	T1Command(Thread1Container& _t1Cont) :
		ThreadCommand(ThreadType::TT_Thread1), t1Cont(_t1Cont)
	{}
	virtual bool execute() const = 0;
};

class T1SubtractCommand : public T1Command {
public:
	int left, right;
	T1SubtractCommand(Thread1Container& _t1Cont, int _left, int _right) :
		T1Command(_t1Cont), left(_left), right(_right)
	{}
	virtual bool execute() const;
};

class T1TerminateCommand : public T1Command {
public:
	T1TerminateCommand(Thread1Container& _t1Cont) :
		T1Command(_t1Cont)
	{}
	virtual bool execute() const;
};

#endif